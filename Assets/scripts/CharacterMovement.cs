﻿// same as import
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterMovement : MonoBehaviour {

	// attributes and how to expose them
	// primitives and Unity classes attributes can be exposed to the editor
	public float speed = 5;
    public Text textito;

    private Transform t;
    private GameObject go;



	// lifecycle 
	// - several methods that fit into a particular event in the life of code

	// life of a component in Unity
	// engine works as a loop
	// the logic we do is going to fit this loop

	void Awake () {
		// run once at the very beginning of this component's life
		Debug.Log("AWAKE");
        
        //print ("AWAKE");
        //textito.text = "HEY GUYS, TESTING";


	}

	// Use this for initialization
	void Start () {
		// runs once after ALL the awake invokations
		print ("START");

        // retrieve a reference to another component in the same game object
        //t = GetComponent<Transform>();

        // retrieve a reference to another game object
        go = GameObject.Find("Capsule");
        t = go.GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		// runs once per frame
		// FPS - frames per second
		// real time - 30 fps
		// update is run once per frame

		// this guy is important for performance
		// try to limit this for 2 things - 
		// - input: responsiveness 
		// - motion: as smooth as possible
		// print is a non-blocking operation
		//print("UPDATE");

		// 2 main ways to deal with the input
		// -polling directly to devices
		if(Input.GetKeyDown(KeyCode.A)){
			print ("DOWN");
		}

		if (Input.GetKey (KeyCode.A)) {
			print ("KEY");
		}

		if (Input.GetKeyUp (KeyCode.A)) {
			print ("UP");
		}

		if (Input.GetMouseButtonDown (0)) {
			print ("MOUSE DOWN");
		}


		// -polling through axes
		// plural of axis
		// a way in which the engine abstracts input

		// the result of polling an axis is a float value
		// range [-1, 1]
		float h = Input.GetAxis("Horizontal");

		//print (h);

		// motion
		// several ways to move things
		// - affect position directly
		// - move through physics engine
		t.Translate(speed * h * Time.deltaTime, 0, 0, Space.World);

	}

	void LateUpdate() {
		// also happen on each frame, after ALL updates are done
		//print("LATE UPDATE");
	}

	void FixedUpdate(){
		// runs on a set framerate
		// used if you want something to happen on a set timeframe
		//print("FIXED");
	}

    // collisions with the physics engine
    // requirements 
    // - at least 2 objects (duh)
    // - both objects have a collider
    // - at least one object has a rigidbody
    // - rigidbody must be moving

    // detect 3 states of collision
    // this method is only invoked at the beginning of the collision
    private void OnCollisionEnter(Collision choquecito)
    {
        print(choquecito.transform.name);
    }

    // while the objects keep touching
    private void OnCollisionStay(Collision collision)
    {
        print("STAY");
    }

    // when they stop touching
    private void OnCollisionExit(Collision collision)
    {
        print("EXIT");
    }

    private void OnTriggerEnter(Collider other)
    {
        print("ENTER");
    }

    private void OnTriggerStay(Collider other)
    {
        print("STAY");
    }

    private void OnTriggerExit(Collider other)
    {
        print("EXIT");
    }
}
